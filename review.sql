-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 30, 2019 at 01:46 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `review`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `email`, `password`) VALUES
(1, 'a', 'a@a.com', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `aid` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `contact` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`aid`, `password`, `fname`, `lname`, `contact`) VALUES
('chris@example.com', 'chris', 'Chris', 'Mount', 'john@example.com'),
('john@example.com', 'john', 'John', 'Doe', 'john@example.com'),
('lewis@example.com', 'lewis', 'John', 'Lewis', 'john@example.com');

-- --------------------------------------------------------

--
-- Table structure for table `have`
--

CREATE TABLE `have` (
  `id` int(11) NOT NULL,
  `aid` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `have`
--

INSERT INTO `have` (`id`, `aid`, `time`) VALUES
(1, 'chris@example.com', '2019-01-29 23:43:52'),
(1, 'john@example.com', '2019-01-29 23:43:52'),
(2, 'john@example.com', '2019-01-29 23:44:41'),
(2, 'lewis@example.com', '2019-01-29 23:44:41'),
(4, 'doe@example.com', '2019-01-30 06:43:04'),
(4, 'john@example.com', '2019-01-30 06:43:04');

-- --------------------------------------------------------

--
-- Table structure for table `paper`
--

CREATE TABLE `paper` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `abstract` varchar(50) NOT NULL,
  `file` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paper`
--

INSERT INTO `paper` (`id`, `title`, `abstract`, `file`) VALUES
(1, 'Network intrusion attack', 'Security', 'attack'),
(2, 'Cipher text', 'Cryptography', 'cipher'),
(4, 'Security review of Wireguard', 'cryptography', 'wireguard');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(11) NOT NULL,
  `rid` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `feedback` varchar(50) DEFAULT NULL,
  `committee` varchar(50) DEFAULT NULL,
  `tech` int(11) DEFAULT '-1',
  `reliable` int(11) DEFAULT '-1',
  `original` int(11) DEFAULT '-1',
  `relevance` int(11) DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `rid`, `status`, `feedback`, `committee`, `tech`, `reliable`, `original`, `relevance`) VALUES
(1, 'chris@example.com', 'pending', NULL, NULL, -1, -1, -1, -1),
(1, 'collin@example.com', 'pending', NULL, NULL, -1, -1, -1, -1),
(2, 'mount@example.com', 'pending', NULL, NULL, -1, -1, -1, -1),
(4, 'collin@example.com', 'reviewed', 'Add more details', 'Good', 10, 8, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `reviewer`
--

CREATE TABLE `reviewer` (
  `rid` varchar(255) NOT NULL,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `phone` int(10) DEFAULT NULL,
  `interest` varchar(50) DEFAULT NULL,
  `affiliation` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviewer`
--

INSERT INTO `reviewer` (`rid`, `fname`, `lname`, `phone`, `interest`, `affiliation`) VALUES
('1', 'q', 'r', 123, 'c', 'd'),
('a@a.com', 'a', 'b', 12345, 'cryptography', 'toc'),
('b@a.com', 'b', 'a', 2147483647, 'Cryptography', 'Professor'),
('chris@example.com', 'Chris', 'Brown', 123456798, 'Security', 'Professor'),
('collin@example.com', 'Collin', 'James', 1234567980, 'Security', 'Professor'),
('lewis@example.com', 'lewis', 'collin', 1234567890, 'Security', 'Professor'),
('mount@example.com', 'David', 'Mount', 1234567890, 'Cryptography', 'Associate Professor');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`aid`),
  ADD UNIQUE KEY `email` (`aid`);

--
-- Indexes for table `have`
--
ALTER TABLE `have`
  ADD PRIMARY KEY (`id`,`aid`);

--
-- Indexes for table `paper`
--
ALTER TABLE `paper`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`,`rid`);

--
-- Indexes for table `reviewer`
--
ALTER TABLE `reviewer`
  ADD PRIMARY KEY (`rid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `paper`
--
ALTER TABLE `paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
