<?php session_start();
require '../mysqlConnect.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Conference Review</title>
    <link rel="icon" href="../../assets/img/car.png">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href="../../assets/css/style.css" rel="stylesheet">
		<link href="../../assets/css/style-responsive.css" rel="stylesheet">
		<link href="../../assets/css/bootstrap.css" rel="stylesheet">
		<link href="../../lib/dataTables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
		<link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link rel="stylesheet" href="../../lib/dataTables/js/reports-plugins/buttons.dataTables.min.css"/>

  </head>

  <body>

  <section id="container" >
      <!--header start-->
      <header class="header black-bg">
            <!--logo start-->
            <a href="../../index.php" class="logo"><b>Home</b></a>
            <!--logo end-->

        </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">

                    <p class="centered"><a href="#"><img src="../../assets/img/admin.png" class="img-circle" width="60"></a></p>
                    <h5 class="centered"> <?php echo $_SESSION['email']; ?></h5>

                  <li class="mt">
                      <a href="../admin/admin.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
				<div class="row">
	                  <div class="col-md-12">
	                  <div class="content-panel">
				<h2>Pending reviewers list</h2>

              <table class="table table-bordered" >
                      <thead>
                      <tr>
											<th>S/N</th>
                      <th>Paper ID </th>
											<th>Email address</th>
											<th>Full name</th>
											<th>Contact no.</th>
                      </tr>
                      </thead>
<?php
$sel="SELECT r.id, r.rid, re.phone, re.fname, re.lname FROM `requests` as r, `reviewer` as re where r.status='pending' and r.rid = re.rid";
$run=mysqli_query($con,$sel);
$i=0;
while($row=mysqli_fetch_array($run)){
		$id=$row['id'];
		$rid=$row['rid'];
		$fname=$row['fname'];
		$lname=$row['lname'];
		$contact=$row['phone'];
		$i++;
?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $id; ?></td>
<td ><?php echo $rid; ?></strong></td>
<td><?php echo ($fname . ' ' . $lname); ?></strong></td>
<td ><?php echo $contact; ?></strong></td>
</tr>
<?php }?>
</table>
			</div><!--/content-panel -->
	</div><!-- /col-md-12 -->
	</div>

		</section><!--wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->

      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->

    <script src="../../assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../../assets/js/common-scripts.js"></script>

    <!--script for this page-->

  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

     <!-- DATA TABLE SCRIPTS -->
        <script type="text/javascript" src="../../lib/dataTables/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../lib/dataTables/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../dataTables/js/jquery.dataTables.min.js"></script>
        <script src="../../dataTables/js/reports-plugins/dataTables.buttons.min.js"></script>
        <script src="../../dataTables/js/reports-plugins/jszip.min.js"></script>
        <script src="../../dataTables/js/reports-plugins/pdfmake.min.js"></script>
        <script src="../../dataTables/js/reports-plugins/vfs_fonts.js"></script>
        <script src="../../dataTables/js/reports-plugins/buttons.flash.min.js"></script>
        <script src="../../dataTables/js/reports-plugins/buttons.html5.min.js"></script>
        <script src="../../dataTables/js/reports-plugins/buttons.print.min.js"></script>
        <script>
          function loadData(){

             $(".table").DataTable({
                 dom: 'Bflirt',
                 buttons: [
                     'excel','pdf', 'print'
                 ]
             });
          }
          document.onready= function (){
                loadData();
            }
    </script>
  </body>
</html>
