<?php
require '../mysqlConnect.php';
session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Car parking</title>
    <link rel="icon" href="../../assets/img/admin.jpg" />

    <!-- Bootstrap core CSS -->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="../../assets/css/style-responsive.css" rel="stylesheet">


<!-- Bootstrap Core CSS -->
<link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="../../lib/jquery/jquery.min.js"></script>
<script src="../../lib/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
  <section id="container" >

      <header class="header black-bg">
            <a href="../.../index.php" class="logo"><b>Home</b></a>
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                  <li><a class="logout" href="logout.php" style="background-color:#000000; font-weight: bold;">Logout</a></li>
            	</ul>
            </div>
        </header>

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">

              <ul class="sidebar-menu" id="nav-accordion">
								<p class="centered"><a href="#"><img src="../../assets/img/admin.png" class="img-circle" width="60"></a></p>
								<!-- <h5 class="centered"> <?php echo $_SESSION['email']; ?></h5> -->
								<li class="mt">
										<a href="../admin/admin.php"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
								</li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i>  </h3>
          	<div class="row mt">
          		<div class="col-lg-12">
	              <form class="form-horizontal" action="upload.php" method="POST" enctype="multipart/form-data">
								<div class="form-group">
          			<div class="col-sm-10">
            			<input type="text" class="form-control"  placeholder="Reviewer ID" name="rid" required="true">
          			</div>
        			</div>
  			      <div class="form-group">
        			  <div class="col-sm-10">
            			<input type="text" class="form-control"  placeholder="First name" name="fname" required="true">
          			</div>
        			</div>
        			<div class="form-group">
								<div class="col-sm-10">
            			<input type="text" class="form-control"  placeholder="Last name" name="lname" required="true">
          			</div>
		          </div>
        <div class="form-group">
          <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="Phone" name="phone" required="true">
          </div>
        </div>
				<div class="form-group">
          <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="Interest" name="interest" required="true">
          </div>
        </div>
				<div class="form-group">
          <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="Affiliation" name="affiliation" required="true">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-6 col-sm-10">
            <button type="submit" class="btn btn-default" name="submit">Submit</button>
          </div>
        </div>
      </form>
      </div>
    </div>
		</section>
	</section><!-- /MAIN CONTENT -->

<!--main content end-->
</section>
<!-- js placed at the end of the document so the pages load faster -->
<script src="script.js"></script>
<script src="../../assets/js/jquery.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="../../assets/js/jquery.ui.touch-punch.min.js"></script>
<script class="include" type="text/javascript" src="../../assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../../assets/js/jquery.scrollTo.min.js"></script>
<script src="../../assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--common script for all pages-->
<script src="../../assets/js/common-scripts.js"></script>

<!--script for this page-->
</body>
</html>
